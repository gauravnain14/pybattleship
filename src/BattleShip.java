public class BattleShip implements Ship{
    private int x,y;
    private String name;
    public BattleShip(){this.x=0; this.y=0;}
    public BattleShip(int x, int y){this.x=x; this.y=y;}
    public int getX() {return x;}
    public int getY() {return y;}
    public String getName() {return name;}
    public void setX(int x) {this.x= x;}
    public void setY(int y) {this.y= y;}
    public void setName(String name){this.name=name;}
    public String toString() {return "Name:"+name+", Location:["+x+", "+y+ "]";}
}
