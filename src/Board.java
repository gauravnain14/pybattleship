public class Board {
    private char[][] boardPlay;
    public Board(){
        this.boardPlay = new char[10][10];
        for(int i = 0; i < boardPlay.length; i++)
            for(int j = 0; j < boardPlay[0].length; j++)
                this.boardPlay[i][j] = '~';
    }

    @Override
    public String toString() {
        int x, y, size=boardPlay.length;
        String temp = "   ";
        String space = " ";
        String newLine = "\n";
        for(x=0;x<size;x++){
            if(x>=8)
                space = "";
            temp=temp.concat((x+1)+space);
        }
        temp = temp.concat(newLine);
        space = "  ";
        for(x=0;x<size;x++){
            if(x == 9)
                space = " ";
            temp=temp.concat((x+1)+space);
            for (y=0;y<size;y++)
                temp=temp.concat(boardPlay[x][y]+" ");
            temp = temp.concat(newLine);
        }
        return temp;
    }
}
