public interface Ship {
    public int getX();
    public int getY();
    public String getName();
    public void setX(int x);
    public void setY(int y);
    public void setName(String name);
}
